const vscode = require('vscode');
const fs = require('fs')
const path = require('path');
const cp = require('child_process')
const tempWrite = require('temp-write');
const os = require('os');

let timeout = undefined;
const configuration = vscode.workspace.getConfiguration('squirrel-linter', undefined)

let compiler = "";
function SetCompiler() {
    if (os.type() === "Windows_NT") { //if (path.sep === "\\") { //"Windows"
        if (configuration.get('Version') == "3.1") {
            compiler = '"' + path.join(__dirname, "sq-compiler_3.1_Windows_x86.exe") + '"';
        }
        else if (configuration.get('Version') == "3.2") {
            compiler = '"' + path.join(__dirname, "sq-compiler_3.2_Windows_x86.exe") + '"';
        }
        else if (configuration.get('Version') == "G2O") {
            compiler = '"' + path.join(__dirname, "sq-compiler_G2O_Windows_x86.exe") + '"';
        }
    } else if (os.type() === "Linux") { //if (path.sep === "\/") { //Linux
        if (os.arch() === "x86") {
            if (configuration.get('Version') == "3.1") {
                compiler = '"' + path.join(__dirname, "sq-compiler_3.1_Linux_x86.exe") + '"';
            }
            else if (configuration.get('Version') == "3.2") {
                compiler = '"' + path.join(__dirname, "sq-compiler_3.2_Linux_x86.exe") + '"';
            }
        }
        else if (os.arch() === "x64") {
            if (configuration.get('Version') == "3.1") {
                compiler = '"' + path.join(__dirname, "sq-compiler_3.1_Linux_x64.exe") + '"';
            }
            else if (configuration.get('Version') == "3.2") {
                compiler = '"' + path.join(__dirname, "sq-compiler_3.2_Linux_x64.exe") + '"';
            }
            else if (configuration.get('Version') == "G2O") {
                compiler = '"' + path.join(__dirname, "sq-compiler_G2O_Linux_x64") + '"';
            }
        }
    }
    else { //Other if (path.sep === ":") { //MACOS
        compiler = '"' + path.join(__dirname, "sq-compiler_3.2_Windows_x86.exe") + '"';
    }
}

/**
 * 
 * @param {vscode.ExtensionContext} context 
 */
function activate(context) {
    SetCompiler();
    const compilerDiagnostics = vscode.languages.createDiagnosticCollection("compiler");
    context.subscriptions.push(compilerDiagnostics);
    subscribeToDocumentChanges(context, compilerDiagnostics);
    if (GetG2OServerExecutableFile()) {
        if (configuration.get('Version') != "G2O") {
            let answer1 = "Yes";
            let answer2 = "No";
            vscode.window.showInformationMessage("G2O server file detected. Do you want to switch to G2o squirrel language version?", answer1, answer2).then(function (output) {
                if (output == answer1) {
                    configuration.update('Version', "G2O", vscode.ConfigurationTarget.Global);
                    SetCompiler();
                }
            })
        }
    }
    else {
        if (configuration.get('Version') == "G2O") {
            let answer1 = "Yes";
            let answer2 = "No";
            vscode.window.showInformationMessage("No G2O server file detected. Do you want to switch to general squirrel language version?", answer1, answer2).then(function (output) {
                if (output == answer1) {
                    configuration.update('Version', "3.2", vscode.ConfigurationTarget.Global)
                    SetCompiler();
                }
            })
        }
    }
    function refreshDiagnostics(doc) {
        if (timeout) {
            clearTimeout(timeout);
            timeout = undefined;
        }
        timeout = setTimeout(function () {
            if (path.extname(doc.fileName) === '.nut') {
                let diagnostics = [];
                try {
                    const fileTemp = tempWrite.sync(doc.getText(), 'bak.nut');
                    cp.execSync(compiler + ' "' + fileTemp + '"');
                } catch (err) {
                    if (err) {
                        var myArray;
                        var regex = /Error in (.*) on line (\d+) column (\d+):\s(.*)/gm;
                        while (myArray = regex.exec(err.stderr.toString())) {
                            let range = new vscode.Range(new vscode.Position(Number(myArray[2]) - 1, (myArray[3]) - 1), new vscode.Position((myArray[2]) - 1, (myArray[3]) - 1));
                            diagnostics.push(new vscode.Diagnostic(range, myArray[4], vscode.DiagnosticSeverity.Error));
                        }
                    }
                    try {
                        fs.unlinkSync(fileTemp);
                    } catch (err) { }
                }
                compilerDiagnostics.set(doc.uri, diagnostics);
            }
        }, 200)
    }

    function subscribeToDocumentChanges(context, compilerDiagnostics) {
        if (vscode.window.activeTextEditor) {
            refreshDiagnostics(vscode.window.activeTextEditor.document);
        }
        context.subscriptions.push(
            vscode.window.onDidChangeActiveTextEditor(editor => {
                if (editor) {
                    refreshDiagnostics(editor.document);
                }
            })
        );

        context.subscriptions.push(vscode.workspace.onDidChangeTextDocument(e => {
            refreshDiagnostics(e.document);
        }));

        context.subscriptions.push(vscode.workspace.onDidCloseTextDocument(doc =>
            compilerDiagnostics.delete(doc.uri))
        );
    }

    function GetG2OServerExecutableFile() {
        let temp = fs.readdirSync(vscode.workspace.workspaceFolders[0].uri.fsPath);
        for (const iterator of temp) {
            if (iterator.includes("G2O_Server")) {
                return path.join(vscode.workspace.workspaceFolders[0].uri.fsPath, iterator)
            }
        }
        return undefined
    };

    context.subscriptions.push(vscode.workspace.onDidChangeConfiguration(event => {
        let affected = event.affectsConfiguration("squirrel-linter");
        if (affected) {
            vscode.commands.executeCommand("workbench.action.restartExtensionHost");
        }
    }))
}

function deactivate() { }

module.exports = {
    activate,
    deactivate
}