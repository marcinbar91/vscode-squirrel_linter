[![Marketplace Version](https://img.shields.io/visual-studio-marketplace/v/marcinbar.vscode-squirrel-linter.svg)](https://marketplace.visualstudio.com/items?itemName=marcinbar.vscode-squirrel-linter)
[![Installs](https://img.shields.io/visual-studio-marketplace/i/marcinbar.vscode-squirrel-linter.svg)](https://marketplace.visualstudio.com/items?itemName=marcinbar.vscode-squirrel-linter)
[![Rating](https://img.shields.io/visual-studio-marketplace/r/marcinbar.vscode-squirrel-linter.svg)](https://marketplace.visualstudio.com/items?itemName=marcinbar.vscode-squirrel-linter)

# Squirrel Language Linter #

* Linter support for squirrel language
![linter](https://bitbucket.org/marcinbar91/vscode-squirrel_linter/raw/7a27808618fe618d4c655d56e9166507cdd9b76e/images/linter.gif)

# License
You are free to use this in any way you want, in case you find this useful or working for you but you must keep the copyright notice and license. (MIT)

# Credits
* squirrel-compiler from mabako <https://github.com/mabako/squirrel-compiler>